const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Produto = sequelize.define('Produto', {
    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Nome_Do_Fornecedor: {
        type: DataTypes.STRING,
        allowNull: false
    },
    validade: {
        type: DataTypes.DATEONLY,
        allowNull: true
    },
    ID_Produto: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Preço: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    quantidade: {
        type: DataTypes.INTEGER,
        allowNull: true
    },

})

Produto.associate = function (models) {
    Produto.belongsTo(models.Cliente);
}

module.exports = Produto;