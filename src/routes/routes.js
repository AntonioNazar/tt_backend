const Express = require('express');
const router = Express();
const ClienteController = require('../controllers/ClienteController')
const ProdutoController = require('../controllers/ProdutoController')
    
//rotas para cliente
router.post('/Cliente', ClienteController.create);
router.get('/Cliente/:id', ClienteController.show);
router.get('/Cliente', ClienteController.index);
router.put('/Cliente/:id', ClienteController.update);
router.delete('/Cliente/:id', ClienteController.destroy);

//rotas para produtos
router.post('/Produto', ProdutoController.create);
router.get('/Produto/:id', ProdutoController.show);
router.get('/Produto', ProdutoController.index);
router.put('/Produto/:id', ProdutoController.update);
router.delete('/Produto/:id', ProdutoController.destroy);

//rotas para relação cliente-usuario
router.put('/Cliente/:ClienteId/Produto/:ProdutoId', ProdutoController.addProduto);
router.put('/Cliente/:ClienteId/Produto/:ProdutoId', ProdutoController.removeProduto);

module.exports = router;