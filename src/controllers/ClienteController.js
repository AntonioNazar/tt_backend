const Cliente = require('../models/Cliente');

async function create(req, res) {
    try {
        const cliente = await Cliente.create(req.body);
        return res.status(201).json({ message: "Cliente criado", Cliente: cliente });
    } catch (err){
        return res.status(500).json(err);
    }
}

async function index(req, res) {
    try {
        const clientes = await Cliente.findAll();
        return res.status(200).json({clientes});
    } catch (err){
        return res.status(500).json(err);
    }
}

async function show(req, res) {
    const { id } = req.params;
    try {
        const cliente = await Cliente.findByPk(id);
        return res.status(200).json({cliente});
    } catch (err){
        return res.status(500).json(err);
    }
}

async function update(req, res) {
    const { id } = req.params;
    try {
        const [updated] = await Cliente.update(req.body,{where: {id:id}});
        if (updated) {
            const cliente = await Cliente.findByPk(id);
            return res.status(200).json({ cliente });
        }
    } catch (err){
        return res.status(500).json(err);
    }
}

async function destroy(req, res) {
    const { id } = req.params;
    try {
        const [deleted] = await Cliente.destroy({where: {id:id}});
        if (deleted) {
            return res.status(200).json("Cliente deletado");
        }
    } catch (err){
        return res.status(500).json("Usuario nao encontrado");
    }
}

module.exports = {
    create,
    index,
    show,
    update,
    destroy
}