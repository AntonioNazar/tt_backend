const Produto = require('../models/Produto');
const Cliente = require('../models/Cliente');

async function create(req, res) {
    try {
        const produto = await Produto.create(req.body);
        return res.status(201).json({ message: "produto criado", Produto: produto });
    } catch (err){
        return res.status(500).json(err);
    }
}

async function index(req, res) {
    try {
        const produtos = await Produto.findAll();
        return res.status(200).json({produtos});
    } catch (err){
        return res.status(500).json(err);
    }
}

async function show(req, res) {
    const { id } = req.params;
    try {
        const produto = await Produto.findByPk(id);
        return res.status(200).json({produto});
    } catch (err){
        return res.status(500).json(err);
    }
}

async function update(req, res) {
    const { id } = req.params;
    try {
        const [updated] = await Produto.update(req.body,{where: {id:id}});
        if (updated) {
            const produto = await Produto.findByPk(id);
            return res.status(200).json({ produto });
        }
    } catch (err){
        return res.status(500).json(err);
    }
}

async function destroy(req, res) {
    const { id } = req.params;
    try {
        const [deleted] = await Produto.destroy({where: {id:id}});
        if (deleted) {
            return res.status(200).json("Produto deletado");
        }
    } catch (err){
        return res.status(500).json("Produto nao encontrado");
    }
}

async function addProduto(req, res) {
    const { ClienteId, ProdutoId } = req.params;
    try {
        const cliente = Cliente.findByPk(ClienteId);
        const produto = Produto.findByPk(ProdutoId);
        await produto.setCliente(cliente);
        return res.status(200).json(produto);
    } catch(err) {
        return res.status(500).json({ err });
    }
}

async function removeProduto(req, res) {
    const { ClienteId, ProdutoId } = req.params;
    try {
        const cliente = Cliente.findByPk(ClienteId);
        const produto = Produto.findByPk(ProdutoId);
        await produto.setCliente(null);
        return res.status(200).json(produto);
    } catch(err) {
        return res.status(500).json({ err });
    }
}

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addProduto,
    removeProduto
}